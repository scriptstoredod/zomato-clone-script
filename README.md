<div dir="ltr" style="text-align: left;" trbidi="on">
<span style="background-color: white; box-sizing: border-box; font-family: &quot;noto sans&quot;, sans-serif; font-size: 13px; font-style: inherit; line-height: 2;">Websites like Zomato Clone and Food Panda Clone, however has made business search very simple. Any new startup can also make this possible with a well-organized database optimized to give most relevant results for any query.</span><br />
<span style="background-color: white; box-sizing: border-box; font-family: &quot;noto sans&quot;, sans-serif; font-size: 13px; font-style: inherit; line-height: 2;"><br /></span>
<span style="background-color: white; box-sizing: border-box; font-family: &quot;noto sans&quot;, sans-serif; font-size: 13px; font-style: inherit; line-height: 2;"><a href="http://scriptstore.in/product/zomato-clone-script/" target="_blank">
<button class="single_add_to_cart_button btn btn-default " type="button">
<i class="fa fa-hand-o-right"></i>User Demo</button></a></span><br />
<br />

<button class="single_add_to_cart_button btn btn-default " type="button"><a href="http://scriptstore.in/product/zomato-clone-script/">
<i class="fa fa-hand-o-right"></i>Document</a></button><br />
<br />
<h2 style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 17px; line-height: 1.1; margin: 0px 0px 20px; text-transform: uppercase;">
PRODUCT DESCRIPTION</h2>
<div style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin-bottom: 10px;">
<strong style="box-sizing: border-box;">GENERAL FEATURES:</strong></div>
<div style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin-bottom: 10px;">
Placement of order<br style="box-sizing: border-box;" />Notifications to both parties<br style="box-sizing: border-box;" />Stage of confirmation<br style="box-sizing: border-box;" />Preparation of food<br style="box-sizing: border-box;" />Delivery gets assigned<br style="box-sizing: border-box;" />Delivery of food</div>
<div style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin-bottom: 10px;">
<strong style="box-sizing: border-box;">UNIQUE FEATURES:</strong></div>
<div style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin-bottom: 10px;">
Easy Search<br style="box-sizing: border-box;" />Search Engine Optimized<br style="box-sizing: border-box;" />User Generated Content<br style="box-sizing: border-box;" />User engagement<br style="box-sizing: border-box;" />Beneficial for Businesses<br style="box-sizing: border-box;" />Advertising Option for Businesses<br style="box-sizing: border-box;" />Additional revenue sources</div>
<div style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin-bottom: 10px;">
<strong style="box-sizing: border-box;">USER PANEL:</strong></div>
<ul style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin: 0px; padding: 0px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Login</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Forgot password&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Registration</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Search</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Wallet</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Sms details</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Email log details</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Payment page</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Products</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Online shopping</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Quick launch</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">All-in-one platform</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">One-time payment</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Check refund status</em></li>
</ul>
<div style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin-bottom: 10px;">
<br /></div>
<div style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin-bottom: 10px;">
<strong style="box-sizing: border-box;">ADMIN:</strong></div>
<ul style="background-color: white; box-sizing: border-box; font-family: &quot;Roboto Condensed&quot;, sans-serif; font-size: 14px; margin: 0px; padding: 0px;">
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Login</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Profile</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Admin management</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Product images and videos</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">User management</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Product booking</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Item management</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Cancel item</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Payment management</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Cancellation policies</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Sms log details</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Email log details</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Control panel</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Booking report</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Reports</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Wallet deposit/print /cancel</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">Statistics</em></li>
<li style="box-sizing: border-box; list-style-position: inside;"><em style="box-sizing: border-box;">General settings</em></li>
</ul>
<div>
<span style="font-family: Roboto Condensed, sans-serif;"><span style="font-size: 14px;"><i><b><br /></b></i></span></span></div>
<div>
<span style="font-family: Roboto Condensed, sans-serif;"><span style="font-size: 14px;"><i><b>Check out products:</b></i></span></span></div>
<div>
<span style="color: #9c9c9c; font-family: Roboto Condensed, sans-serif;"><span style="font-size: 14px;"><i><br /></i></span></span></div>
<div>
<span id="docs-internal-guid-3e7508b6-c628-5514-d070-0fbcd7072db8"><div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<span style="font-family: Arial; font-size: 11pt; font-variant-east-asian: normal; font-variant-numeric: normal; vertical-align: baseline; white-space: pre-wrap;"><a href="https://www.doditsolutions.com/zomato-clone/">https://www.doditsolutions.com/zomato-clone/</a></span></div>
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<span style="font-family: Arial; font-size: 11pt; font-variant-east-asian: normal; font-variant-numeric: normal; vertical-align: baseline; white-space: pre-wrap;"><a href="http://phpreadymadescripts.com/zomato-clone-script.html">http://phpreadymadescripts.com/zomato-clone-script.html</a></span></div>
<div dir="ltr" style="line-height: 1.38; margin-bottom: 0pt; margin-top: 0pt;">
<span style="font-family: Arial; font-size: 11pt; font-variant-east-asian: normal; font-variant-numeric: normal; vertical-align: baseline; white-space: pre-wrap;"><a href="http://scriptstore.in/product/zomato-clone-script/">http://scriptstore.in/product/zomato-clone-script/</a></span></div>
<div>
<span style="font-family: Arial; font-size: 11pt; font-variant-east-asian: normal; font-variant-numeric: normal; vertical-align: baseline; white-space: pre-wrap;"><br /></span></div>
</span></div>
</div>
